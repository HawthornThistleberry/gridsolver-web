package GridSolver;

import java.util.Random;

public class WordGrid {

  /*
  This class implements a four-by-four Boggle-style letter grid.
  It has constructors to create grids (either a standard grid, a
  specified grid, or a random one), print the grid, extract a word
  from a series of positions, and most importantly, to create a
  list of all words found in the grid (using adjacent letters without
  repeating a tile) that are also in another word list.
  */

  public static final int GRID_SIZE = 4;

  protected char[] TheGrid;
  WordList AllTheWords;

  //////////////////////////////////////////////////////////////////////////
  // Constructor that loads a specified sixteen characters into the grid.
  public WordGrid(WordList AllTheWords, String gridLetters) {
    this.AllTheWords = AllTheWords;
    
    TheGrid = new char[GRID_SIZE*GRID_SIZE];
    
    if (gridLetters.equals("random")) {
      Random rand = new Random();
      for (int i = 0; i < GRID_SIZE*GRID_SIZE ; i++ )
      {
        TheGrid[i] = (char) (rand.nextInt(26) + 97);
      }
      return;
    } 
    
    if (gridLetters.equals("boggle")) {
      for (int i = 0; i < GRID_SIZE*GRID_SIZE ; i++ )
      {
        TheGrid[i] = BoggleRandom(false, i);
      }
      return;
    }

    if (gridLetters.equals("classic")) {
      for (int i = 0; i < GRID_SIZE*GRID_SIZE ; i++ )
      {
        TheGrid[i] = BoggleRandom(true, i);
      }
      return;
    }

    if (gridLetters.length() != GRID_SIZE*GRID_SIZE) {
      gridLetters = "DGHIKLPSYEUTEORN";
    }
    for (int i = 0; i < GRID_SIZE*GRID_SIZE ; i++ )
    {
      TheGrid[i] = Character.toLowerCase(gridLetters.charAt(i));
    }
  }

  private String BoggleDie(boolean classic, int die) {
    if (classic) {
      switch(die) {
        case 0: return "aaciot";
        case 1: return "abilty";
        case 2: return "abjmoq";
        case 3: return "acdemp";
        case 4: return "acelrs";
        case 5: return "adenvz";
        case 6: return "ahmors";
        case 7: return "biforx";
        case 8: return "denosw";
        case 9: return "dknotu";
        case 10: return "eefhiy";
        case 11: return "egkluy";
        case 12: return "egintv";
        case 13: return "ehinps";
        case 14: return "elpstu";
        case 15: return "gilruw";
      }
    } else {
      switch(die) {
        case 0: return "aaeegn";
        case 1: return "abbjoo";
        case 2: return "achops";
        case 3: return "affkps";
        case 4: return "aoottw";
        case 5: return "cimotu";
        case 6: return "deilrx";
        case 7: return "delrvy";
        case 8: return "distty";
        case 9: return "eeghnw";
        case 10: return "eeinsu";
        case 11: return "ehrtvw";
        case 12: return "eiosst";
        case 13: return "elrtty";
        case 14: return "himnuq";
        case 15: return "hlnnrz";
      }
    }
    return "??????";
  }

  private char BoggleRandom(boolean classic, int die) {
    Random rand = new Random();
    String thisDie = BoggleDie(classic, die);
    return thisDie.charAt(rand.nextInt(6));
  }

  //////////////////////////////////////////////////////////////////////////
  // Method to output the grid as an HTML table
  public String HTMLDisplayGrid() {
    String theResult = "<table border=1>";
    for (int row = 0; row < GRID_SIZE; row++)
    {
      theResult += "<tr>";
      for (int col = 0; col < GRID_SIZE; col++)
      {
        char thisLetter = TheGrid[row * GRID_SIZE + col];
        theResult += "<td align=center width=50 height=50><font size='+2'>";
        theResult += Character.toUpperCase(thisLetter);
        if (thisLetter == 'q') theResult += 'u';
        theResult += "<font></td>";
      }
      theResult += ("</tr>");
    }
    theResult += "</table>";
    return theResult;
  }


  //////////////////////////////////////////////////////////////////////////
  // Method that returns the string from a list of positions.
  private String WordFromPositions(int[] Positions) {
    StringBuilder result = new StringBuilder();
    for (int i = 0; i < Positions.length; i++)
    {
      char thisLetter = TheGrid[Positions[i]];
      result.append(thisLetter);
      if (thisLetter == 'q') result.append('u');
    }
    return result.toString();
  }

  //////////////////////////////////////////////////////////////////////////
  // Method that returns the new position from a starting position and
  // direction, or -1 if this would take us outside the grid; this is
  // used internally in the grid solver.
  private int AdjacentPosition(int OldPosition, int Direction) {
    // convert a position to row and column
    int row = OldPosition / GRID_SIZE;
    int col = OldPosition % GRID_SIZE;

    // an array is faster than doing the math, and this is
    // inside a frequently-executed function so a good place
    // to throw away a few dozen bytes to gain that speed
    int[] DirectionRowDelta = {-1, -1, -1,  0, 0,  1, 1, 1};
    int[] DirectionColDelta = { -1, 0, 1, -1, 1, -1, 0, 1 };

    // shift
    row += DirectionRowDelta[Direction];
    col += DirectionColDelta[Direction];

    // throw a -1 if we're now outside the grid
    if (row < 0 || row >= GRID_SIZE || col < 0 || col >= GRID_SIZE) return -1;

    // convert back to position
    return row * GRID_SIZE + col;
  }

  //////////////////////////////////////////////////////////////////////////
  // A private method to check an integer array for a value already present,
  // used here to ensure that we aren't reusing a tile
  // (Java probably provides a native method to do this)
  private boolean ValueInArray(int value, int[] IntArray) {
    for (int i = 0; i < IntArray.length; i++ )
    {
      if (IntArray[i] == value) return true;
    }
    return false;
  }

  //////////////////////////////////////////////////////////////////////////
  // Given a series of positions used so far and a direction, returns the
  // position in that direction, but only if it's a valid position not
  // already present in the list; this is used while recursing to avoid
  // going around in loops and thus having endless recursion.
  private int AdjacentUnusedPosition(int[] Positions, int Direction) {
    int NewPosition = AdjacentPosition(Positions[Positions.length-1], Direction);
    if (NewPosition == -1) return -1;
    if (ValueInArray(NewPosition, Positions)) return -1;
    return NewPosition;
  }

  //////////////////////////////////////////////////////////////////////////
  // For a given list of positions already explored, report it if it's a word,
  // then see if adding one more position will make a word in each possible
  // direction (excluding crossing back over places you've already been,
  // or falling off the grid). This is a function that will recurse extensively,
  // and is where the main work of the program is done.
  private String RecursePosition(int[] Positions, WordList AllTheWords) {
    String result = "";

    // First, if this is a word, report it to the output.
    String AllegedWord = WordFromPositions(Positions);
    if (AllTheWords.WordInList(AllegedWord)) result += AllegedWord + " ";
    // Doing this first ensures single-letter words are found (e.g., I)

    // If the grid allows the same word to be created by differing
    // paths, it will be reported each time. For instance, in
    // the supplied grid, LYE can be made two ways (5 8 9 and 5 8 12)
    // and so will be listed twice. I consider this a feature, but
    // if it's considered a bug, you could always pipe the output
    // into uniq! (Or solve it with an internal cache, but that's a
    // much less modular solution, so I haven't done that here.)

    // Next, if the list is already sixteen elements, take an early exit
    // Note: if there are no valid directions, we won't recurse anyway,
    // so this is just an optimization, and the code would work without
    // it (ideally we'd prove this with red-green refactoring).
    if (Positions.length == GRID_SIZE*GRID_SIZE) return result;

    // Another optimization is to abandon a search entirely if nothing starts
    // with the letters we already have.
    if (!AllTheWords.PrefixInList(AllegedWord)) return result;

    // For each possible direction, try adding that direction
    for (int Direction = 0; Direction <= 7; Direction++) {
      int NewPosition = AdjacentUnusedPosition(Positions, Direction);
      if (NewPosition != -1) {
        int NewLength = Positions.length + 1;
        int[] NewPositions = new int[NewLength];
        for (int i = 0; i < Positions.length; i++) NewPositions[i] = Positions[i];
        NewPositions[NewLength-1] = NewPosition;
        result += RecursePosition(NewPositions, AllTheWords);
      }
    }

    return result;
  }

  //////////////////////////////////////////////////////////////////////////
  // This method is the heart of the program: given a grid it will print all
  // the words in it (it relies on the WordList class for the master word
  // list). It relies on RecursePosition to do the real work; it just
  // presents it with 16 starting position lists, each representing a
  // path of a single tile.
  public String FindWordsInGrid() {
    // declare and construct a WordList
    //WordList AllTheWords = new WordList("wordlist.txt");
    String result = "";

    // try solutions starting at each grid location
    for (int StartPosition = 0; StartPosition < GRID_SIZE*GRID_SIZE; StartPosition++)
    {
      int[] Positions = new int[1];
      Positions[0] = StartPosition;
      result += RecursePosition(Positions, AllTheWords);
    }
    return result;
  }
}