package GridSolver;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class WordGridController {
  
  WordGrid myGrid;
  WordList AllTheWords = new WordList("wordlist.txt");
  
  private String BuildFullHTML(String title, String body) {
    return "<html><head><meta charset='utf-8'>" +
      "<meta http-equiv='X-UA-Compatible' content='IE=edge'>" + 
      "<meta name='viewport' content='width=device-width, initial-scale=1'>" +
      "<title>GridSolver - " + title + "</title>" +
      "<link href='css/bootstrap.min.css' rel='stylesheet'></head><body>" +
      "<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>" +
      "<script src='js/bootstrap.min.js'></script>" + body + "</body>";
  }
  
  @RequestMapping("/")
  public String HomePage() {      
      return BuildFullHTML("Home Page", 
        "<p>Welcome to the Grid Solver.</p>" +
        "<ul>" +
        "<li><a href='/solve'>Solve default grid</a></li>" +
        "<li><a href='/solve?letters=boggle'>Solve random grid generated using Boggle dice</a></li>" +
        "<li><a href='/solve?letters=classic'>Solve random grid generated using classic Boggle dice</a></li>" +
        "<li><a href='/solve?letters=random'>Solve random grid using all letters equally</a></li>" +
        "<li><a href='/solve?letters=abcdefghijklmnop'>Solve boring grid</a></li>" +
        "</ul>" +
        "<p>To make your own grid, edit the URL and add /solve?letters= to the end, then type 16 letters and those will fill the grid. " +
        "(For a Qu tile just put the Q.) An interactive grid builder will be added one day..."
      );
  }
  
  @RequestMapping("/solve")
  public String SolveWordGrid(@RequestParam(value="letters", defaultValue="") String gridLetters) {
      myGrid = new WordGrid(AllTheWords, gridLetters);
      String result = myGrid.HTMLDisplayGrid();
      result += "<p>Solutions:</p><p>";
      result += myGrid.FindWordsInGrid();
      result += "</p>";
      return BuildFullHTML("Solve Grid", result);
  }
  
  
  
}